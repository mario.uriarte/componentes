import 'package:flutter/material.dart';

class AvatarPage extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
        title: Text('Avatar Page'),
        actions: <Widget>[
          Container(
            padding: EdgeInsets.all(5.0),
            child: CircleAvatar(
              backgroundImage: NetworkImage('https://i0.pngocean.com/files/591/710/430/super-mario-bros-paper-mario-new-super-mario-bros-mario-bros-thumb.jpg'),
              radius: 25.0,
            ),
          ),
          Container(
            margin: EdgeInsets.only(right: 10.0),
            child: CircleAvatar(
              child: Text('MU'),
              backgroundColor: Colors.brown,
            ),
          )
        ],
      ),
      body: Center(
        child: FadeInImage(
          image: NetworkImage('https://i.pinimg.com/originals/76/a9/8c/76a98cfe60f88be3d23d003b2623e028.jpg'),
          placeholder: AssetImage('assets/preloader_1.gif'),
        ),
      ),
    );

  }
}